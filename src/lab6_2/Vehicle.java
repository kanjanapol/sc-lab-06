package lab6_2;

 abstract class Vehicle {
	public void Startengine() {
		System.out.println("Engine start!!");
	}
	
	public abstract void Showdestination(); // abstract methods

}

class Car extends Vehicle {
	public void Showdestination() { // overiding abstract methods
		System.out.println("go to central");
	}

	public String Showdestination(String go) { // overloading abstract methods

		return go;
	}
}
 class Motorcyle  extends Vehicle{

	@Override
	public void Showdestination() {
		// TODO Auto-generated method stub
		
	}
	
}

class Program {
	public static void main(String args[]) {
		Car car1 = new Car();
		car1.Startengine();
		car1.Showdestination();
		System.out.println("go to " + car1.Showdestination("Hua Hin"));
	}
}