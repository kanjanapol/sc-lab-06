package lab6_1;

public class Car extends Vehicle {
	protected int license_N;
	protected String version;
	protected String color;
	protected int wheel;

	// *public Car(String version, String color) {
	// *this.version = version;
	// *this.color = color;

	// *}

	public Car() {
		this.wheel = super.wheel;
	}

	public Car(String version, String color, int wheel) {
		this.version = version;
		this.color = color;
		this.wheel = wheel;

	}

	public int getwheel() {
		return wheel;
	}
	public String getversion() {
		return version;
	}
	public String getcolor() {
		return color;
	}

}
