package lab6_1;

public class Main {

	public static void main(String[] args) {
		
		Car car1 = new Car(); // constructor from superclass
		Car car2 = new Car("honda"," red", 5); //overload constructor in subclass
		
		
		System.out.println("wheel is  " + car1.getwheel());	
		System.out.println("wheel is  "+ car2.getwheel() + "  "+car2.getversion() + car2.getcolor());		//return wheel of car 

	}
}
